<?php

namespace app\models\light;

use app\models\SitePages;
use yii\base\Component;

class PageModel extends Component
{

    public
        $title,
        $url,
        $descendants = [];

    /**
     * @param SitePages $page
     */
    public function __construct(SitePages $page)
    {
        parent::__construct();

        $this->title = $page->title;
        $this->url = $page->url_full;
        $this->descendants = [];
    }

}