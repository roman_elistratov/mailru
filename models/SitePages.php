<?php
namespace app\models;

use Yii;
use yii\base\InvalidRouteException;
use yii\behaviors\AttributeBehavior;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;
use app\classes\behaviors\ActualUrl;
use app\classes\behaviors\ContentMapping;
use app\models\light\PageModel;

/**
 * @property int $id
 * @property int $parent_id
 * @property string $url
 * @property string $url_full
 * @property string $title
 * @property string $content
 */
class SitePages extends ActiveRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'site_pages';
    }

    /**
     * @return []
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'URL',
            'title' => 'Название',
            'content' => 'Содержание',
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['parent_id',], 'integer'],
            [['title', 'url', 'url_full', 'content',], 'string'],
            [['title', 'url', ], 'required'],
            ['url', 'match', 'pattern' => '#^\w+$#']
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ActualUrl' => ActualUrl::className(),
            'ContentMapping' => ContentMapping::className(),
        ];
    }

    /**
     * @return null|static
     * @throws InvalidRouteException
     * @throws NotFoundHttpException
     */
    public static function findByUrl()
    {
        $url = trim(Yii::$app->request->queryParams['url'], '/');

        if (empty($url)) {
            throw new InvalidRouteException;
        }

        $page = self::findOne(['url_full' => $url]);
        if (is_null($page)) {
            throw new NotFoundHttpException('The requested page does not exist');
        }

        return $page;
    }

    /**
     * @return array
     */
    public function getDescendants()
    {
        $result = [];
        $descendants =
            self::find()
                ->where(['LIKE', 'url_full', $this->url_full . '%', false])
                ->andWhere('parent_id')
                ->andWhere(['!=', 'id', $this->id])
                ->orderBy(['parent_id' => SORT_ASC, 'id' => SORT_ASC])
                ->each();

        foreach ($descendants as $page) {
            if (isset($result[$page->parent_id])) {
                $result[$page->parent_id]->descendants[] = new PageModel($page);
            } else {
                $result[$page->id] = new PageModel($page);
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return 'test';
    }

}