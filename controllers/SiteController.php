<?php

namespace app\controllers;

use Yii;
use yii\base\InvalidRouteException;
use yii\web\Controller;
use app\models\SitePages;
use app\forms\page\PageNewForm;
use app\forms\page\PageEditForm;

class SiteController extends Controller
{

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        try {
            return $this->render('page/view', [
                'page' => $page = SitePages::findByUrl(),
            ]);
        } catch (InvalidRouteException $e) {
            return $this->render('index', [
                'pages' => SitePages::find()->where(['parent_id' => null])->each(),
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAddPage()
    {
        $form = new PageNewForm;

        if ($form->isSaved) {
            Yii::$app->session->setFlash('success', 'The object was created successfully');
            return $this->redirect(['index']);
        }

        return $this->render('page/edit', [
            'formModel' => $form,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionEditPage()
    {
        $form = new PageEditForm;

        if ($form->isSaved) {
            Yii::$app->session->setFlash('success', 'The object was saved successfully');
            return $this->redirect(['index']);
        }

        return $this->render('page/edit', [
            'formModel' => $form,
        ]);
    }

}
