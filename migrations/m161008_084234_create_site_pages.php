<?php

use yii\db\Migration;
use app\models\SitePages;

class m161008_084234_create_site_pages extends Migration
{

    public function up()
    {
        $tableName = SitePages::tableName();

        $this->createTable($tableName, [
            'id' => $this->primaryKey(11),
            'parent_id' => $this->integer(11),
            'url' => $this->string(255),
            'url_full' => $this->string(255),
            'title' => $this->string(255),
            'content' => $this->text(),
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createIndex('url_full', $tableName, 'url_full', $unique = true);
        $this->createIndex('parent_id', $tableName, 'parent_id');
    }

    public function down()
    {
        $tableName = SitePages::tableName();

        $this->dropTable($tableName);
    }

}
