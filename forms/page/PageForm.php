<?php

namespace app\forms\page;

use Yii;
use InvalidArgumentException;
use yii\base\Model;
use app\models\SitePages;

/**
 * @property int $id
 * @property bool $isSaved
 * @property array $validateErrors
 * @property SitePages $record
 * @property string $urlFull
 */
abstract class PageForm extends Model
{

    /** @var SitePages $record */
    public
        $id,
        $isSaved = false,
        $validateErrors = [],
        $record,
        $urlFull = '/';

    /**
     * @return SitePages
     */
    abstract public function getRecordModel();

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->record = $this->getRecordModel();
        $this->loadFromInput();
    }

    /**
     * @throws \yii\db\Exception
     */
    protected function loadFromInput()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $post = Yii::$app->request->post();

            if ($this->record->load($post)) {
                if ($this->record->validate() && $this->record->save()) {
                    $this->id = $this->record->id;
                    $this->isSaved = true;
                } else {
                    $this->validateErrors += $this->record->getFirstErrors();
                }
            }

            if ($this->validateErrors) {
                throw new InvalidArgumentException;
            }

            $transaction->commit();
        } catch (InvalidArgumentException $e) {
            $transaction->rollBack();
            $this->isSaved = false;
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::error($e);
            $this->isSaved = false;
            $this->validateErrors[] = YII_DEBUG ? $e->getMessage() : 'Internal error';
        }
    }
}