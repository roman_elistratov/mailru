<?php

namespace app\forms\page;

use Yii;
use app\models\SitePages;
use yii\base\InvalidRouteException;

class PageNewForm extends PageForm
{

    /**
     * @return SitePages
     */
    public function getRecordModel()
    {
        $page = new SitePages;

        /** @var SitePages $parentPage */
        try {
            $parentPage = SitePages::findByUrl();
            $page->parent_id = $parentPage->id;
            $this->urlFull = $parentPage->url_full;
        } catch (InvalidRouteException $e) {
        }

        return $page;
    }

}