<?php

namespace app\forms\page;

use Yii;
use app\models\SitePages;

class PageEditForm extends PageForm
{

    /**
     * @return SitePages
     */
    public function getRecordModel()
    {
        /** @var SitePages $page */
        $page = SitePages::findByUrl();
        $this->urlFull = $page->url_full;

        return $page;
    }

}