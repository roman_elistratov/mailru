<?php

namespace app\classes\helpers;

use yii\bootstrap\Html;

abstract class ContentMapping
{

    /**
     * @param string $content
     * @return string
     */
    public static function applyMapping($content)
    {
        foreach (self::applyRules() as $pattern => $callback) {
            if (!$callback instanceof \Closure) {
                continue;
            }

            $content = preg_replace_callback('#' . $pattern . '#', $callback, $content);
        }

        return $content;
    }

    public static function clearMapping($content)
    {
        $rules = self::clearRules();
        return preg_replace_callback('#<!--\s?(mapping:\s?(\w+))\s?-->(.*?)<!--\s?/\\1\s?-->#', function($input) use ($rules) {
            $mapKey = $input[2];
            if (isset($rules[$mapKey]) && $rules[$mapKey] instanceof \Closure) {
                return $rules[$mapKey]($input);
            }
            return '';
        }, $content);
    }

    /**
     * @return array
     */
    private static function applyRules()
    {
        return [
            '\*{2}([^\*]+)\*{2}' => function($input) {
                return '<!-- mapping: bold -->' . Html::tag('b', $input[1]) . '<!-- /mapping: bold -->';
            },
            '\\\\\\\\([^\\\\]+)\\\\\\\\' => function($input) {
                return '<!-- mapping: italic -->' . Html::tag('i', $input[1]) . '<!-- /mapping: italic -->';
            },
            '\({2}([\w\/]+)\s([^\)]+)\){2}' => function($input) {
                return '<!-- mapping: link -->' . Html::a($input[2], $input[1]) . '<!-- /mapping: link -->';
            },
        ];
    }

    public static function clearRules()
    {
        return [
            'bold' => function($input) {
                return '**' . strip_tags($input[3]). '**';
            },
            'italic' => function($input) {
                return '\\\\' . strip_tags($input[3]) . '\\\\';
            },
            'link' => function($input) {
                $tag = $input[3];
                preg_match('#<[^>]*href=["\']([^"\']+)["\']>([^>]+)</a>#', $tag, $match);
                return '((' . $match[1] .  ' ' . $match[2] . '))';
            },
        ];
    }

}