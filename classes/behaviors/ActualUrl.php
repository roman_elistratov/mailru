<?php

namespace app\classes\behaviors;

use app\models\SitePages;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;
use yii\db\Expression;

class ActualUrl extends Behavior
{

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'UrlAfterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'UrlBeforeUpdate',
        ];
    }

    /**
     * @param AfterSaveEvent $event
     */
    public function UrlAfterInsert($event)
    {
        $page = $event->sender;
        $page->url_full = $this->getParentUrl($page->parent_id) . $page->url;
        $page->save();
    }

    /**
     * @param AfterSaveEvent $event
     */
    public function UrlBeforeUpdate($event)
    {
        if (isset($event->changedAttributes['url'])) {
            $page = $event->sender;
            $fullUrl = $this->getParentUrl($page->parent_id) . $page->url;

            SitePages::updateAll([
                'url_full' => new Expression('REPLACE(url_full, :before, :after)', [
                    'before' => $page->url_full . DIRECTORY_SEPARATOR,
                    'after' => $fullUrl . DIRECTORY_SEPARATOR
                ])
            ], [
                'LIKE', 'url_full', $page->url_full . DIRECTORY_SEPARATOR . '%', false
            ]);

            $page->url_full = $this->getParentUrl($page->parent_id) . $page->url;
            $page->save();
        }
    }

    /**
     * @param int $parentId
     * @return string
     */
    private function getParentUrl($parentId)
    {
        if (!(int)$parentId) {
            return '';
        }

        /** @var SitePages $parentPage */
        $parentPage = SitePages::findOne($parentId);
        if (!is_null($parentPage)) {
            return $parentPage->url_full . DIRECTORY_SEPARATOR;
        }
    }

}