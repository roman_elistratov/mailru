<?php

namespace app\classes\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;
use app\classes\helpers\ContentMapping as ContentMappingHelper;

class ContentMapping extends Behavior
{

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'ContentMappingBeforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'ContentMappingBeforeSave',
        ];
    }

    /**
     * @param AfterSaveEvent $event
     */
    public function ContentMappingBeforeSave($event)
    {
        $page = $event->sender;
        $page->content = ContentMappingHelper::applyMapping($page->content);
    }

}