<?php

use yii\bootstrap\Html;

/**
 * Вывести submit-кнопку "Сохранить"
 *
 * @var yii\base\View $this
 * @var string $text
 */

!isset($text) && $text = 'Create';
?>

<?= Html::submitButton(
    Html::tag('i', '', [
        'class' => 'glyphicon glyphicon-save',
        'aria-hidden' => 'true',
    ]) . ' ' .
    $text,
    [
        'class' => 'btn btn-primary',
    ]
) ?>