<?php

use yii\bootstrap\Html;

/**
 * Вывести ссылку-кнопку "Отменить"
 *
 * @var yii\base\View $this
 * @var string $text
 * @var string $url
 */

!isset($text) && $text = 'Cancel';
?>

<?= Html::a(
    Html::tag('i', '', [
        'class' => 'glyphicon glyphicon-level-up',
        'aria-hidden' => 'true',
    ]) . ' ' .
    $text,
    $url,
    [
        'class' => 'btn btn-link btn-cancel',
    ]
) ?>
