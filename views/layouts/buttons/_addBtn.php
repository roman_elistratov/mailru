<?php

use yii\bootstrap\Html;

/**
 * ������� ������-������ "��������"
 *
 * @var yii\base\View $this
 * @var string $text
 * @var string $url
 */

!isset($text) && $text = 'Add';
?>

<?= Html::a(
    Html::tag('i', '', [
        'class' => 'glyphicon glyphicon-plus',
        'aria-hidden' => 'true',
    ]) . ' ' .
    $text,
    $url . '/add',
    [
        'class' => 'btn btn-link',
    ]
) ?>