<?php

use yii\bootstrap\Html;

/**
 * ������� ������-������ "�������������"
 *
 * @var yii\base\View $this
 * @var string $text
 * @var string $url
 */

!isset($text) && $text = 'Edit';
?>

<?= Html::a(
    Html::tag('i', '', [
        'class' => 'glyphicon glyphicon-edit',
        'aria-hidden' => 'true',
    ]) . ' ' .
    $text,
    $url . '/edit',
    [
        'class' => 'btn btn-link',
    ]
) ?>