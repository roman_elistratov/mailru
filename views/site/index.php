<?php

use yii\bootstrap\Html;

/** @var yii\web\View $this  */
/** @var app\models\SitePages[] $pages */

?>
<div class="site-index">

    <div class="page-header row">
        <div class="col-sm-10">
            <h1>Index page</h1>
        </div>
        <div class="col-sm-2">
            <?= $this->render('//layouts/buttons/_addBtn', ['url' => false]) ?>
        </div>
    </div>

    <div class="page-container">
        <ul>
            <?php foreach ($pages as $page): ?>
                <li><?= Html::a($page->title, $page->url_full) ?></li>
            <?php endforeach; ?>
        </ul>
    </div>

</div>
