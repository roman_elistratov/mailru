<?php

use yii\bootstrap\Html;

/** @var yii\web\View $this */
/** @var app\models\SitePages[] $pages */
?>

<ul>
    <?php foreach ($pages as $page): ?>
        <li>
            <?= Html::a($page->title, '/' . $page->url) ?>
            <?php if ($page->descendants): ?>
                <?= $this->render('descendants', ['pages' => $page->descendants]) ?>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>
