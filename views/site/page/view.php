<?php

/** @var yii\web\View $this */
/** @var app\models\SitePages $page */

?>
<div class="site-page">

    <div class="page-header row">
        <div class="col-sm-10">
            <h1><?= $page->title ?></h1>
        </div>
        <div class="col-sm-2">
            <?= $this->render('//layouts/buttons/_addBtn', ['url' => '/' . $page->url_full]) ?>
            <?= $this->render('//layouts/buttons/_editBtn', ['url' => '/' . $page->url_full]) ?>
        </div>
    </div>

    <div class="page-container">
        <?= $page->content ?>
    </div>

    <?php if (count($page->descendants)): ?>
        <div class="well">
            <?= $this->render('descendants', ['pages' => $page->descendants]) ?>
        </div>
    <?php endif; ?>
</div>
