<?php

use yii\widgets\ActiveForm;
use app\classes\helpers\ContentMapping;

/** @var app\forms\page\PageForm $formModel */

$page = $formModel->record;
?>

<div class="well">
    <?php
    $form = ActiveForm::begin();
    $viewParams = [
        'formModel' => $formModel,
        'form' => $form,
    ];
    ?>

    <?php
    if ($formModel->validateErrors) {
        Yii::$app->session->setFlash('error', $formModel->validateErrors);
    }
    ?>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($page, 'url')->textInput()->hint('Start from "/' . $formModel->urlFull . '"') ?>
        </div>
        <div class="col-sm-8">
            <?= $form->field($page, 'title')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($page, 'content')->textarea([
                'value' => ContentMapping::clearMapping($page->content),
                'rows' => 20,
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($page, 'parent_id')->hiddenInput()->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <?= $this->render('//layouts/buttons/_submitBtn', ['text' => ($page->isNewRecord ? 'Create' : 'Save')]) ?>
        <?= $this->render('//layouts/buttons/_cancelBtn', ['url' => '/' . $formModel->record->url_full]) ?>
    </div>

    <?php ActiveForm::end() ?>
</div>